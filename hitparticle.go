package main

import (
	"math/rand"
	"time"

	"gitlab.com/onikolas/agl/game"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

type Hit struct {
	sprite   sprite.Sprite
	target   math.Vector3[float32]
	bb       *game.BoundingBox
	duration time.Duration
	game.GameObjectCommon
}

func NewHit(position math.Vector3[float32]) *Hit {
	hit := Hit{}
	position.Z = 4
	hit.SetTranslation(position)
	target := math.Vector3[float32]{1 - 2*rand.Float32(), 1 - 2*rand.Float32(), 0}
	hit.SetTarget(target.Normalize())
	hit.duration = time.Millisecond * 200

	// sprite
	img, err := sprite.RgbaFromFile("data/star.png")
	panicOnError(err)
	id, err := Game.Atlas.AddImage(img)
	panicOnError(err)
	hit.sprite, _ = sprite.NewSprite(id, Game.Atlas, &Game.Shader, 0)
	hit.sprite.SetScale(hit.sprite.GetScale().Scale(1 + 2*rand.Float32()))
	hit.sprite.SetRotation(math.Vector3[float32]{0, 0, VectorToAngle(target.Normalize())})

	hit.bb = Game.Collisions.NewBoundingBox(false, &hit)
	hit.Init()

	return &hit
}

func (f *Hit) Update(dt time.Duration) {
	movement := f.target.Scale(800 * fdt(dt))
	f.SetTranslation(f.GetTranslation().Add(movement))
	f.sprite.SetPosition(f.GetTranslation())
	if !screenBox.Contains(f.GetTranslation().XY()) {
		f.Destroy()
	}
	f.duration -= dt
	if f.duration < 0 {
		f.Destroy()
	}
	f.bb.Update(dt, f)
}

func (f *Hit) Render(r *sprite.Renderer) {
	r.QueueRender(&f.sprite)
}

func (f *Hit) SetTarget(target math.Vector3[float32]) {
	f.target = target.Normalize()
}
