module jam

go 1.21.5

require (
	gitlab.com/onikolas/agl v0.0.0-20240128101733-ee25d86a21df
	gitlab.com/onikolas/math v0.0.0-20231231170231-56ace31a034d
)

require (
	github.com/go-gl/gl v0.0.0-20231021071112-07e5d0ea2e71 // indirect
	github.com/veandco/go-sdl2 v0.4.38 // indirect
	gitlab.com/onikolas/ds v0.0.0-20230808130118-53c60ec78bef // indirect
	golang.org/x/exp v0.0.0-20240119083558-1b970713d09a // indirect
)
