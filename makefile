run:
	go build && ./jam

get-agl:
	GOPROXY=direct go get -u -v 

build-linux:
	CGO_ENABLED=1 CC=gcc GOOS=linux GOARCH=amd64 go build -tags static -ldflags "-s -w"

build-windows:
	CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc GOOS=windows GOARCH=amd64 go build -tags static -ldflags "-s -w"

package: build-linux build-windows
	zip -r laffo.zip jam.exe jam data README.md

