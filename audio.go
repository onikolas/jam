package main

import (
	"time"

	"github.com/veandco/go-sdl2/mix"
)

var audioFiles = map[string]*mix.Music{}

var audioFilenames = map[string]string{
	"anger":           "data/anger.mp3",
	"credits":         "data/credits.mp3",
	"fart00001":       "data/fart00001.mp3",
	"fart0001":        "data/fart0001.mp3",
	"fart001":         "data/fart001.mp3",
	"fart01":          "data/fart01.mp3",
	"fart02":          "data/fart02.mp3",
	"fart1":           "data/fart1.mp3",
	"fart2":           "data/fart2.mp3",
	"fart999":         "data/fart999.mp3",
	"ftth":            "data/ftth.mp3",
	"get-off-my-lawn": "data/get-off-my-lawn.mp3",
	"giggle":          "data/giggle.mp3",
	"laugh":           "data/laugh.mp3",
	"music":           "data/music.mp3",
	"nomnomnom":       "data/nomnomnom.mp3",
	"short fart":      "data/short fart.mp3",
	"stifle":          "data/stifle.mp3",
	"title":           "data/title.mp3",
	"victory":         "data/victory.mp3",
	"clucking":        "data/clucking.mp3",
	"talk":            "data/talk.mp3",
}

func InitAudio() {
	err := mix.Init(mix.INIT_MP3)
	panicOnError(err)

	err = mix.OpenAudio(22050, mix.DEFAULT_FORMAT, 2, 4096)
	panicOnError(err)

	for k, v := range audioFilenames {
		audioFiles[k], err = mix.LoadMUS(v)
		panicOnError(err)
	}
}

var audioCooldown time.Duration

func playAudio(clip string) {
	if audioCooldown <= 0 {
		audioFiles[clip].Play(0)
		audioCooldown = time.Millisecond * 200
	}
}
