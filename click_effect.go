package main

import (
	"time"

	"gitlab.com/onikolas/agl/game"
	"gitlab.com/onikolas/agl/sprite"
)

type ClickEffect struct {
	sprite    sprite.Sprite
	ShowTimer time.Duration
	game.GameObjectCommon
}

func NewClickEffect() *ClickEffect {
	c := ClickEffect{}
	spriteImg, err := sprite.RgbaFromFile("data/pointer.png")
	panicOnError(err)
	spriteId, _ := Game.Atlas.AddImage(spriteImg)
	c.sprite, _ = sprite.NewSprite(spriteId, Game.Atlas, &Game.Shader, 0)
	c.sprite.SetScale(c.sprite.GetScale().Scale(2))
	return &c
}

func (ce *ClickEffect) Update(dt time.Duration) {
	if ce.ShowTimer > 0 {
		ce.sprite.SetPosition(ce.GetTranslation())
		ce.ShowTimer -= dt
	}
}

func (ce *ClickEffect) Render(r *sprite.Renderer) {
	if ce.ShowTimer > 0 {
		r.QueueRender(&ce.sprite)
	}
}
