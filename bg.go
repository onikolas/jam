package main

import (
	"gitlab.com/onikolas/agl/game"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

type Background struct {
	sprite sprite.Sprite
	game.GameObjectCommon
}

func NewBackground() *Background {
	img, err := sprite.RgbaFromFile("data/bg.png")
	panicOnError(err)
	index, err := Game.Atlas.AddImage(img)
	panicOnError(err)
	sprite, err := sprite.NewSprite(index, Game.Atlas, &Game.Shader, 0)
	sprite.SetPosition(math.Vector3[float32]{float32(windowSize.X) / 2, float32(windowSize.Y) / 2, -1})
	sprite.SetScale(sprite.GetScale().Mul(math.Vector2[float32]{2, 2}))
	return &Background{
		sprite: sprite,
	}
}

func (b *Background) Render(r *sprite.Renderer) {
	r.QueueRender(&b.sprite)
}
