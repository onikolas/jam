package main

import (
	"fmt"
	gomath "math"
	"math/rand"
	"time"

	"gitlab.com/onikolas/agl/game"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

var featherSprite = []string{
	"feather1.png", "feather2.png",
}

type Feather struct {
	sprite sprite.Sprite
	target math.Vector3[float32]
	bb     *game.BoundingBox
	game.GameObjectCommon
}

func NewFeather(position, target math.Vector3[float32]) *Feather {
	feather := Feather{}
	position.Z = 2
	feather.SetTranslation(position)
	feather.SetTarget(target.Normalize())

	// sprite
	img, err := sprite.RgbaFromFile("data/" + featherSprite[rand.Int()%2])
	panicOnError(err)
	id, err := Game.Atlas.AddImage(img)
	panicOnError(err)
	feather.sprite, _ = sprite.NewSprite(id, Game.Atlas, &Game.Shader, 0)
	feather.sprite.SetScale(feather.sprite.GetScale().Scale(2))
	feather.sprite.SetRotation(math.Vector3[float32]{0, 0, VectorToAngle(target.Normalize())})

	feather.bb = Game.Collisions.NewBoundingBox(false, &feather)
	feather.Init()

	return &feather
}

func (f *Feather) Update(dt time.Duration) {
	movement := f.target.Scale(800 * fdt(dt))
	f.SetTranslation(f.GetTranslation().Add(movement))
	f.sprite.SetPosition(f.GetTranslation())
	if !screenBox.Contains(f.GetTranslation().XY()) {
		f.Destroy()
	}
	f.bb.Update(dt, f)
	collisions := f.bb.Check()
	for i := range collisions {
		if game.HasTag(collisions[i], TagWeakspot) {
			oldman := collisions[i].(*Hurtbox).GetParent().(*Oldman)
			oldman.MoodUp()
			fmt.Println("Mood ", oldman.GetMood(), " feather ", f.Id())
			f.Destroy()
			playAudio("giggle")
			Game.Scene.AddGameObject(NewHit(f.GetTranslation()))
			Game.Scene.AddGameObject(NewHit(f.GetTranslation()))
			Game.Scene.AddGameObject(NewHit(f.GetTranslation()))
			audioCooldown = time.Second
		}
	}
}

func (f *Feather) Render(r *sprite.Renderer) {
	r.QueueRender(&f.sprite)
}

func (f *Feather) SetTarget(target math.Vector3[float32]) {
	f.target = target.Normalize()
}

func VectorToAngle(vector math.Vector3[float32]) float32 {
	return float32(gomath.Atan2(float64(vector.Y), float64(vector.X)))
}
