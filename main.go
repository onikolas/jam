package main

import (
	"time"

	"gitlab.com/onikolas/agl/color"
	"gitlab.com/onikolas/agl/game"
	"gitlab.com/onikolas/agl/platform"
	"gitlab.com/onikolas/agl/shaders"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

var windowSize = math.Vector2[int]{1280, 720}
var screenBox = math.Box2D[float32]{}.New(0, 0, float32(windowSize.X), float32(windowSize.Y))
var screenBoxSlim = math.Box2D[float32]{
	P1: screenBox.P1.Add(math.Vector2[float32]{64, 64}),
	P2: screenBox.P2.Sub(math.Vector2[float32]{64, 64}),
}

var Game struct {
	Atlas      *sprite.Atlas
	Shader     shaders.Shader
	Input      *platform.StateManager
	Collisions game.CollisionManagerAABB
	Scene      *game.Scene
}

func panicOnError(e error) {
	if e != nil {
		panic(e)
	}
}

func StartScreen() {
	Game.Scene.AddGameObject(NewScreen("data/title2.png", "title", Credits))
}

func Credits() {
	Game.Scene.AddGameObject(NewScreen("data/credits1.png", "credits", Instructions))
}

func Instructions() {
	Game.Scene.AddGameObject(NewScreen("data/instructions.png", "talk", MainGame))
}

func MainGame() {
	Game.Scene.AddGameObject(NewBird())
	Game.Scene.AddGameObject(NewBackground())
	Game.Scene.AddGameObject(NewOldman())
}

func main() {
	err := platform.InitializeWindow(1280, 720, "Laffo The Chicken", true, false)
	if err != nil {
		panic(err)
	}
	InitAudio()

	renderer := sprite.NewRenderer()
	bgColor := color.NewColorRGBAFromBytes(40, 50, 40, 255)
	renderer.SetBGColor(bgColor.ToArray())

	Game.Atlas, err = sprite.NewEmptyAtlas(2000, 2000)
	if err != nil {
		panic(err)
	}
	Game.Shader, _ = shaders.NewDefaultShader()
	Game.Input = platform.NewStateManager()
	Game.Scene = &game.Scene{}
	StartScreen()
	Game.Atlas.DumpAtlas()

	Game.Scene.Init()

	timer := time.Now()
	for {
		dt := time.Since(timer)
		audioCooldown -= dt
		timer = time.Now()
		Game.Input.GetState()
		Game.Scene.Update(dt)
		Game.Scene.Render(renderer)
		renderer.Render()
	}
}
