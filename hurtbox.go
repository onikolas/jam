package main

import (
	"image"
	"image/color"
	"image/draw"
	"time"

	"gitlab.com/onikolas/agl/game"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

type Hurtbox struct {
	sprite sprite.Sprite
	bb     *game.BoundingBox
	parent game.GameObject
	game.GameObjectCommon
	live bool
}

var hurtboxVisibility = 0 //150

func NewHurtbox(position math.Vector3[float32], size math.Vector2[float32], tag int, parent game.GameObject) *Hurtbox {
	m := image.NewRGBA(image.Rect(0, 0, 4, 4))
	blue := color.RGBA{0, 0, 255, uint8(hurtboxVisibility)}
	if tag == TagHurtbox {
		blue = color.RGBA{255, 0, 0, uint8(hurtboxVisibility)}
	}
	draw.Draw(m, m.Bounds(), &image.Uniform{blue}, image.ZP, draw.Src)
	index, err := Game.Atlas.AddImage(m)
	panicOnError(err)
	sprite, err := sprite.NewSprite(index, Game.Atlas, &Game.Shader, 1)
	h := &Hurtbox{sprite: sprite}
	h.SetTranslation(position)
	h.SetScale(size)
	h.bb = Game.Collisions.NewBoundingBox(true, h)
	h.live = true
	h.SetEmbed(h)
	h.AddTag(tag)
	h.parent = parent
	return h
}

func (h *Hurtbox) Update(dt time.Duration) {
	h.sprite.SetPosition(h.GetTranslation())
	h.sprite.SetScale(h.GetScale())
	h.bb.Update(dt, h)
}

func (h *Hurtbox) Render(r *sprite.Renderer) {
	if h.live {
		r.QueueRender(&h.sprite)
	}
}

func (h *Hurtbox) GetParent() game.GameObject {
	return h.parent
}
