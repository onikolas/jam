package main

import (
	"os"
	"time"

	"gitlab.com/onikolas/agl/game"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

var omSprites = [][]string{
	{
		"data/angry0.png",
		"data/angry1.png",
		"data/angry2.png",
		"data/angry3.png",
		"data/angry4.png",
	},
	{
		"data/hurt0.png",
		"data/hurt1.png",
		"data/hurt2.png",
		"data/hurt3.png",
		"data/hurt4.png",
	},
	{
		"data/sad0.png",
		"data/sad1.png",
		"data/sad2.png",
		"data/sad3.png",
		"data/sad4.png",
	},
	{
		"data/neutral0.png",
		"data/neutral1.png",
		"data/neutral2.png",
		"data/neutral3.png",
		"data/neutral4.png",
	},
	{
		"data/smirk0.png",
		"data/smirk1.png",
		"data/smirk2.png",
		"data/smirk3.png",
		"data/smirk4.png",
	},
	{
		"data/smile0.png",
		"data/smile1.png",
		"data/smile2.png",
		"data/smile3.png",
		"data/smile4.png",
	},
	{
		"data/laugh0.png",
		"data/laugh1.png",
		"data/laugh2.png",
		"data/laugh3.png",
		"data/laugh4.png",
	},
}

type Oldman struct {
	animation          game.Animation
	clips              []int
	currentClip        int
	mood, moodInterval int
	weakPoints         []*Hurtbox
	weakPointPos       []math.Vector3[float32]
	hurtbox            *Hurtbox
	hurtboxPos         math.Vector3[float32]
	moveVector         math.Vector3[float32]
	getOffMyLawned     bool
	game.GameObjectCommon
}

func NewOldman() *Oldman {
	om := Oldman{}
	om.animation = game.NewAnimation()
	om.moodInterval = 10

	for _, v := range omSprites {
		indices, err := Game.Atlas.AddImagesFromFiles(v)
		panicOnError(err)
		clip := om.animation.AddClip(indices, Game.Atlas, &Game.Shader, 0)
		om.clips = append(om.clips, clip)
	}

	om.animation.Run()
	om.animation.SetClip(om.clips[0])
	om.animation.SetAnimationSpeed(4)

	om.moveVector = math.Vector3[float32]{-1, 0, 0}

	om.SetTranslation(math.Vector3[float32]{920, 240, 0})
	spr := om.animation.GetSprite(om.clips[0], 0)
	spriteSize := math.Vector2ConvertType[int, float32](spr.GetOriginalSize())
	om.SetScale(spriteSize.Scale(2))

	// weak points
	om.weakPoints = append(om.weakPoints, NewHurtbox(om.GetTranslation(), math.Vector2[float32]{32, 32}, TagWeakspot, &om))
	om.AddChild(om.weakPoints[0])
	om.weakPointPos = append(om.weakPointPos, math.Vector3[float32]{-70, 30, 0})
	om.weakPoints = append(om.weakPoints, NewHurtbox(om.GetTranslation(), math.Vector2[float32]{32, 32}, TagWeakspot, &om))
	om.AddChild(om.weakPoints[1])
	om.weakPointPos = append(om.weakPointPos, math.Vector3[float32]{70, 30, 0})

	// hurtbox
	om.hurtbox = NewHurtbox(om.GetTranslation(), math.Vector2[float32]{180, 280}, TagHurtbox, &om)
	om.AddChild(om.hurtbox)
	om.hurtboxPos = math.Vector3[float32]{0, -50, 0}

	return &om
}

func (o *Oldman) Update(dt time.Duration) {
	if !o.getOffMyLawned {
		playAudio("get-off-my-lawn")
		o.getOffMyLawned = true
		audioCooldown = 4 * time.Second
	}

	ot := o.GetTranslation()
	o.SetTranslation(ot.Add(o.moveVector.Scale(90 * fdt(dt))))
	if ot.X < 250 {
		o.moveVector = math.Vector3[float32]{1, 0, 0}
	}
	if ot.X > float32(windowSize.X)-250 {
		o.moveVector = math.Vector3[float32]{-1, 0, 0}
	}
	o.animation.Update(dt, o)
	for i := range o.weakPoints {
		o.weakPoints[i].Update(dt)
		ot.Z = 3
		o.weakPoints[i].SetTranslation(ot.Add(o.weakPointPos[i]))
	}
	o.hurtbox.Update(dt)
	o.hurtbox.SetTranslation(ot.Add(o.hurtboxPos))
}

func (o *Oldman) Render(r *sprite.Renderer) {
	o.animation.Render(r)
}

func (o *Oldman) MoodUp() {
	o.mood += 1
	if o.mood%o.moodInterval == 0 {
		o.currentClip += 1
		if o.currentClip == 7 {
			os.Exit(0) // TODO show credits
		}
		o.animation.SetClip(o.currentClip)
	}
}

func (o *Oldman) GetMood() int {
	return o.mood
}
