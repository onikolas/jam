package main

import (
	"math/rand"
	"os"
	"time"

	"gitlab.com/onikolas/agl/game"
	"gitlab.com/onikolas/agl/platform"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

type Bird struct {
	animation                               game.Animation
	idleClip, flyClip, attackClip, ouchClip int
	shotCooldown                            float32
	moveVector                              math.Vector3[float32]
	bb                                      *game.BoundingBox
	hp                                      float32
	game.GameObjectCommon
}

var chickenIdleFrames = [2]string{
	"data/chicken1.png",
	"data/chicken2.png",
}

var chickenFlyFrames = [4]string{
	"data/chickenfart.png",
	"data/chicken1.png",
	"data/chickenfart1.png",
	"data/chicken2.png",
}

var chickenAttackFrames = [1]string{
	"data/chickenA.png",
}

var chickenOuchFrames = [3]string{
	"data/chicken1.png",
	"data/chickendamage.png",
	"data/chicken1.png",
}

func NewBird() *Bird {
	bird := Bird{}
	bird.animation = game.NewAnimation()

	// idle
	indices, err := Game.Atlas.AddImagesFromFiles(chickenIdleFrames[:])
	panicOnError(err)
	bird.idleClip = bird.animation.AddClip(indices, Game.Atlas, &Game.Shader, 0)

	// fly
	indices, err = Game.Atlas.AddImagesFromFiles(chickenFlyFrames[:])
	panicOnError(err)
	bird.flyClip = bird.animation.AddClip(indices, Game.Atlas, &Game.Shader, 0)

	// attack
	indices, err = Game.Atlas.AddImagesFromFiles(chickenAttackFrames[:])
	panicOnError(err)
	bird.attackClip = bird.animation.AddClip(indices, Game.Atlas, &Game.Shader, 0)

	// ouch
	indices, err = Game.Atlas.AddImagesFromFiles(chickenOuchFrames[:])
	panicOnError(err)
	bird.ouchClip = bird.animation.AddClip(indices, Game.Atlas, &Game.Shader, 0)

	bird.animation.Run()
	bird.animation.SetClip(bird.idleClip)
	bird.animation.SetAnimationSpeed(4)

	// starting position and size
	bird.SetTranslation(math.Vector3[float32]{500, 500, 1})
	spr := bird.animation.GetSprite(bird.idleClip, 0)
	spriteSize := math.Vector2ConvertType[int, float32](spr.GetOriginalSize())
	bird.SetScale(spriteSize.Scale(2))

	// bounding box setup
	bird.bb = Game.Collisions.NewBoundingBox(false, &bird)
	bird.hp = 10

	return &bird
}

func (b *Bird) Update(dt time.Duration) {
	fdt := float32(dt.Seconds())

	// mouse movement (hold LMB to move)
	if Game.Input.MouseButtonDown(platform.MouseLeft) {
		direction := Game.Input.MousePosition().Sub(b.GetTranslation().XY()).Normalize()
		b.moveVector = b.moveVector.Add(direction.AddZ(0).Scale(0.2 * fdt))
		if direction.Y > 0 {
			b.animation.SetClip(b.flyClip)
			fart := rand.Int() % 8
			switch fart {
			case 1:
			case 2:
			case 3:
			case 4:
				playAudio("short fart")
			case 5:
				playAudio("fart001")
			case 6:
				playAudio("fart0001")
			case 7:
				playAudio("fart00001")
			}
		}
	} else {
		b.animation.SetClip(b.idleClip)
	}

	// gravity
	b.moveVector = b.moveVector.Add(math.Vector3[float32]{0, -1, 0}.Scale(0.1 * fdt))

	if b.moveVector.X < 0 {
		b.faceLeft()
	} else if b.moveVector.X > 0 {
		b.faceRight()
	}

	// boundaries
	bpos := b.GetTranslation()
	b.SetTranslation(bpos.Add(b.moveVector))
	if b.GetTranslation().Y < 64 {
		bpos.Y = 64
		b.SetTranslation(bpos)
		b.moveVector.Y = -b.moveVector.Y / 2
	}
	if b.GetTranslation().Y > float32(windowSize.Y)-64 {
		bpos.Y = float32(windowSize.Y) - 64
		b.SetTranslation(bpos)
		b.moveVector.Y = 0
	}
	bpos = b.GetTranslation()
	if b.GetTranslation().X < 0 || b.GetTranslation().X > float32(windowSize.X)-64 {
		bpos.X = math.Bound[float32](bpos.X, 0, float32(windowSize.X)-64)
		b.SetTranslation(bpos)
		b.moveVector.X = -b.moveVector.X / 2
	}

	// shoot feathers with RMB
	if Game.Input.MouseButtonDown(platform.MouseRight) && b.shotCooldown < math.Epsilon {
		target := Game.Input.MousePosition().AddZ(0).Sub(b.GetTranslation()).Normalize()
		Game.Scene.AddGameObject(NewFeather(b.GetTranslation(), target))
		b.shotCooldown = 0.2
		b.animation.SetClip(b.attackClip)
		playAudio("ftth")
	}
	if b.shotCooldown >= math.Epsilon {
		b.shotCooldown -= fdt
	}

	// check if we hit something
	if hits := b.bb.Check(); len(hits) > 0 {
		for i := range hits {
			if game.HasTag(hits[i], TagHurtbox) {
				b.animation.SetClip(b.ouchClip)
				b.hp -= fdt
				if b.hp < 0 {
					os.Exit(0)
				}
			}
		}
	}

	b.animation.Update(dt, b)
	b.bb.Update(dt, b)
}

func (b *Bird) Render(r *sprite.Renderer) {
	b.animation.Render(r)
}

func (b *Bird) faceLeft() {
	b.SetRotation(math.Vector3[float32]{0, 3.14, 0})
}

func (b *Bird) faceRight() {
	b.SetRotation(math.Vector3[float32]{0, 0, 0})
}
