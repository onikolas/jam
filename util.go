package main

import "time"

func fdt(t time.Duration) float32 {
	return float32(t.Seconds())
}
