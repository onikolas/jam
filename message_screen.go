package main

import (
	"time"

	"gitlab.com/onikolas/agl/game"
	"gitlab.com/onikolas/agl/platform"
	"gitlab.com/onikolas/agl/sprite"
	"gitlab.com/onikolas/math"
)

type Screen struct {
	sprite   sprite.Sprite
	next     func()
	duration time.Duration
	game.GameObjectCommon
	music  string
	played bool
}

func NewScreen(file, music string, next func()) *Screen {
	img, err := sprite.RgbaFromFile(file)
	panicOnError(err)
	index, err := Game.Atlas.AddImage(img)
	panicOnError(err)
	sprite, err := sprite.NewSprite(index, Game.Atlas, &Game.Shader, 0)
	sprite.SetPosition(math.Vector3[float32]{float32(windowSize.X) / 2, float32(windowSize.Y) / 2, -1})
	sprite.SetScale(sprite.GetScale().Mul(math.Vector2[float32]{2, 2}))

	return &Screen{
		sprite:   sprite,
		next:     next,
		duration: 3 * time.Second,
		music:    music,
	}
}

func (b *Screen) Render(r *sprite.Renderer) {
	r.QueueRender(&b.sprite)
}

func (s *Screen) Update(dt time.Duration) {
	if !s.played {
		playAudio(s.music)
		s.played = true
	}

	s.duration -= dt
	if Game.Input.MouseButtonDown(platform.MouseLeft) && s.duration < 0 {
		s.next()
		s.Destroy()
	}
}
